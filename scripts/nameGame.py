# creates a verse based on the rules set by the song Name Game by Shirley Ellis

name = input("What is your name? ").lower()

if name[0] in ['b','f','m']:
    print("{}, {}, bo-{}".format(name.capitalize(), name.capitalize(), name[1:]))
    print("Bonana-fanna fo-{}".format(name[1:]))
    print("Fee fi mo-{}".format(name[1:]))
    print(name.capitalize())
elif name[0] in ['a','e','i','o','u']:
    print("{}, {}, bo-b{}".format(name.capitalize(), name.capitalize(), name))
    print("Bonana-fanna fo-f{}".format(name))
    print("Fee fi mo-m{}".format(name))
    print(name.capitalize())
else:
    print("{}, {}, bo-b{}".format(name.capitalize(), name.capitalize(), name[1:]))
    print("Bonana-fanna fo-f{}".format(name[1:]))
    print("Fee fi mo-m{}".format(name[1:]))
    print(name.capitalize())
