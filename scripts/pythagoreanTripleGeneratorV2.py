#euler75
#using Dickson's method

import math, fractions, collections

limit = 1500000

#generates an L such that a right triangle is formed
def generator(limit):
	triple = set()
	for i in range(3, math.ceil(math.sqrt(limit)), 2):
		for j in range((i-2), 0, -2):
			if fractions.gcd(i,j) == 1:
				x = i*j
				y = (i**2-j**2)/2
				z = (j**2+i**2)/2
				total = x + y + z
				triple.add(total)
	return triple

nums = set(generator(limit))
temp = set()
for num in nums:
	if num > limit:
		temp.add(num)
#perform a difference between two sets to give a set where all values < limit
solutions = nums - temp
hurrah = []
#all solutions are a multiple of a previous solution (i.e (3,4,5,12) -> (6,8,10,24))
#appends all multiples of solutions to a new list
for solution in solutions:
	for i in range(int(solution), limit+1, int(solution)):
		hurrah.append(i)
#increments up for each time a value occurs in hurrah for each value
d = collections.defaultdict(int)
for x in hurrah: d[x] += 1
hurrah[:] = [x for x in hurrah if d[x] == 1]
counter = 0
for v in d.values():
	if v == 1:
		counter += 1
print(counter)
