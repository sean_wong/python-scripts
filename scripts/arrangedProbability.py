#euler100
#deriving numBlue from (numBlue/(numBlue+numRed) * ((numBlue-1)/(numBlue+numRed-1))
#returns numBlue = numRed + ((sqrt(8numRed**2+1)+1)/2)
#for increasing numRed, numBlue is only a whole number during certain values (1,6,35)

import math

numRed = 1 #(3 blue, 1 red)
while(True):
	#check if numBlue will return whole number
	if (math.sqrt(8*(numRed**2)+1)+1) % 2 == 0:
		numBlue = numRed + (math.sqrt(8*numRed**2+1)+1)/2
		print(numBlue,numRed)
	numRed += 1
	if (numRed + numBlue) > 1000000000000:
		break

print(numBlue)
