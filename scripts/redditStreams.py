#Reddit Stream Aggregation
#command-line program to aggregate sports streams on reddit
#Sean Wong, Joseph Delly

import requests
from bs4 import BeautifulSoup
import webbrowser

# Dictionary with keys of supported sports games that the program can retrieve
# with corresponding url values to the streaming subreddits
SPORTS = {'NBA': 'https://www.reddit.com/r/nbastreams/', 
          'NFL': 'https://www.reddit.com/r/nflstreams/',
          'MMA': 'https://www.reddit.com/r/mmastreams/',
          'MLB': 'https://www.reddit.com/r/mlbstreams/',
          'NHL': 'https://www.reddit.com/r/nhlstreams/',
          'Soccer': 'https://www.reddit.com/r/redsoccer/'}

def selectSport():
        """Prompts the user with a sport selection menu from the key,value pairs in the SPORTS dict.
        Returns the subreddit url of the selected sport taken from the SPORTS dict."""
        print('\nSelect a sport:')
        for i, key in enumerate(SPORTS):
                print(str(i + 1) + ") " + key)
        print('7) Exit/Quit\n')
        while True:
                try:
                        response = int(input()) - 1
                        if response == 6:
                                print('goodbye :)')
                                quit()
                        elif response < 0:
                                raise IndexError
                        return list(SPORTS.values())[response]
                except (IndexError, ValueError, TypeError):
                        print('invalid input, enter your response again: ', end='')

def getRedditPost(subreddit):
        """Queries the given subreddit and parses the source code for sport streaming posts.
        Returns a truple of a list of post urls, and a list of comment urls."""
        keyword = 'game_thread' 
        exclude = 'game_thread_'
        http = 'https://www.reddit.com/r/'
        source = BeautifulSoup(requests.get(subreddit, headers = {'User-Agent': 'Mozilla/5.0'}).text , 'html.parser')
        urls = [line.get('href') for line in source.find_all('a')]
        commentUrls = [x for x in urls if keyword in x]
        postUrls = [i.split(exclude, 1)[1] for i in commentUrls if http in i]
        return (sorted(set(postUrls), key = postUrls.index), sorted(set(commentUrls), key = commentUrls.index))

def getGameUrls(postUrls, commentUrls):
        """Prompts the user to select a specific sports game. Accepts a postUrl and commentUrl list
        and parses the source code for external websites posted in the comment section. Restricts specfic 
        urls using a hardcoded filter."""
        reddit = 'https://www.reddit.com'
        print('\nSelect a game:')
        for i, game in enumerate(postUrls):
                print(str(i + 1) + ') ' + game)
        print()
        while True:
                try:
                        response = int(input()) - 1
                        if response > len(postUrls) or response < 0:
                                raise IndexError
                        source = BeautifulSoup(requests.get(commentUrls[response], headers = {'User-Agent': 'Mozilla/5.0'}).text, 'html.parser')
                        links = [line.get('href') for line in source.find_all('a')]
                        externalLinks = [link for link in links if link != '/' and
                                                                   'reddit' not in link and
                                                                   'user' not in link and
                                                                   '/r/' not in link and
                                                                   '/u/' not in link and
                                                                   'png' not in link and
                                                                   'php' not in link and
                                                                   'original' not in link and
                                                                   'discord' not in link]
                        return sorted(set(externalLinks), key = externalLinks.index)
                except (IndexError, ValueError, TypeError):
                        print('invalid input, enter your response again: ', end='')

def selectGame(gameUrls):
        """Prompts the user to select from a list of gameUrls, and opens the target url in the primary browser."""
        print('Select a url to open: ')
        for i, url in enumerate(gameUrls):
                print(str(i + 1) + ') ' + url)
        print()
        while True:
                try:
                        response = int(input()) - 1
                        if response > len(gameUrls) or response < 0:
                                raise IndexError
                        webbrowser.open(gameUrls[response])
                except (IndexError, ValueError, TypeError):
                        print('invalid input, enter your response again: ', end='')

if __name__ == '__main__':
       subreddit = selectSport()
       postUrls, commentUrls = getRedditPost(subreddit)
       gameUrls = getGameUrls(postUrls, commentUrls)
       selectGame(gameUrls)
