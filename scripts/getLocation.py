from urllib import request
import json

try:
    response = json.load(request.urlopen('http://ip-api.com/json/'))
except Exception:
    print("There has been an error. Please try again.")

print("IP Address: {}".format(response['query']))
print("City: {}".format(response['city']))
print("Latitude: {}".format(response['lat']))
print("Longitude: {}".format(response['lon']))
