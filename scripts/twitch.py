import bs4
from selenium import webdriver
import time
import os
import sys

url = "https://twitch.tv"

def menu():
	print("""What would you like to do?
1. Check most popular games
2. View a genre of games
3. Quit
	""")
	option = input()
	return int(option)

#what = 'div', {'class': 'tw-mg-b-2 tw-relative'}
#what = 'p', {'class': 'tw-c-text-alt tw-ellipsis'}) 

def getData(url, tag, attribute):
	instance = webdriver.PhantomJS()
	instance.get(url)
	i = 0
	while(i < 10):
		directory = instance.page_source
		htmlData = bs4.BeautifulSoup(directory, 'html.parser')
		data = htmlData.findAll(tag, attribute)
		if data != []:
			break
		time.sleep(1)
	instance.quit()
	return data

def watch(item):
	return input("Enter the number of the {} you want to see or enter nothing to go back to the menu: ".format(item))

def getPopular():
	os.system('clear')
	gameList = []
	tag = "div"
	attribute = {'class': 'tw-mg-b-2 tw-relative'}
	games = getData((url+"/directory"), tag, attribute)
	for i in range(0,len(games)):
		print(str(i+1) + ".) " + games[i].h3.text + " has " + games[i].p.text)
		gameList.append(games[i].a['href'])		
	return gameList

def getStreamers(game):
	os.system('clear')
	streamerList = []
	tag = "div"
	attribute = {'class': 'tw-mg-b-2 tw-relative'}
	streamers = getData((url+game), tag, attribute)
	for i in range(0,len(streamers)):
		print(str(i+1) + ".) " + streamers[i].div.div.div.next_sibling.p.a.text + " has " + streamers[i].p.text + " and is streaming at " + "\n" + (url + streamers[i].a['href']) + "\n")
		streamerList.append(streamers[i].a['href'])
	return streamerList		

def main():
	response = menu()
	if response == 1:
		gameList = getPopular()
		gameChoice = watch("game")
		if gameChoice == "":
			main()
		streamerList = getStreamers(gameList[int(gameChoice)-1])
		streamerChoice = watch("streamer")
		if streamerChoice == "":
			main()
		if sys.platform != 'linux':
			import webbrowser
			webbrowser.open(url + streamerList[int(streamerChoice)-1])
			quit()
		os.system("sudo mpv {}".format(url + streamerList[int(streamerChoice)-1]))
	elif response == 3:
		quit()
	else:
		print("That is not a valid option.")
		main()

if __name__ == "__main__":
	main()
