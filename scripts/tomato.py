# pomodoro technique
# 11//3/17

import os
import time

def menu():
    print("Default duration for the task timer is 25 minutes.\nDefault duration for the short break is 5 minutes.\nDefault duration for the long break is 20 minutes.")
    print("Enter nothing to enable defaults.")
    try:
        work = int(input("Enter task duration (1-60 minutes): "))
        if work not in range(1,61):
            raise IndexError
    except(ValueError, IndexError): # sets default time if time not integer 1 < = x <= 60
        work = 25
    try:
        rest = int(input("Enter short break duration (1-60 minutes): "))
        if rest not in range(1,61):
            raise IndexError
    except(ValueError, IndexError):
        rest = 5
    try:
        recess = int(input("Enter long break duration (1-60 minutes): "))
        if recess not in range(1,61):
            raise IndexError
    except(ValueError, IndexError):
        recess = 20
    print("Press Control-C at any time to exit the program.")
    return([work, rest, recess])

def timer():
    messages = ["Work on the task for {} minutes.", "Take a break for {} minutes.", "Take an extended break for {} minutes."]
    print(messages[phase_indicator].format(times[phase_indicator]))
    time.sleep(times[phase_indicator]*60)
    os.system("mpv" + " ~/Downloads/alarm.mp3")

if __name__ == "__main__":
    times = menu()
    while(True):
        phase_indicator = 0 # 0 = work, 1=rest, 2=recess
        break_counter = 0   # 4th break is extended
        while(break_counter != 3):
            if phase_indicator == 0:
                timer()
                phase_indicator = 1
            else:
                timer()
                phase_indicator = 0
                break_counter += 1
        phase_indicator = 2
        timer()
