#euler 125
#generate sums of consecutive squares, check if palindromes
#NOTE 1 = 1**2 + 0**2 IS NOT INCLUDED
#perfect squares do not count

import math

palindromes = set()
limit = 100000000
#limit = 1000

def isPalindrome(x):
	x = str(x)
	if x == x[::-1]:
		return True

def consecutiveSquares(x):
	for i in range(1, math.ceil(math.sqrt(x))+1):
		square = i**2
		for j in range(i+1, math.ceil(math.sqrt(x))+1):
			square += j**2
			if isPalindrome(square) and square < x:
				palindromes.add(square)
			#square += j**2

consecutiveSquares(limit)
print(sorted(palindromes))

print(sum(palindromes))
