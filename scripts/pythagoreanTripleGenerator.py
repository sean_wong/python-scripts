#euler75

import math

def generator(limit):
	triple = []
	total = 0
	j = 4
	while(total < limit):
		for i in range(3,j):
			a = i**2
			b = j**2
			c = math.sqrt(a+b)
			total = i + j + c
			if c.is_integer():
				print(i,j,c,total)
				#triple.append(total)
		j += 1
	return triple

generator(1500000)

#nums = sorted(generator(1500000))
#one_solution = []
#for num in nums:
#	if num > 1500000:
#		nums.remove(num)
#	if num not in one_solution:
#		one_solution.append(num)
#		nums.remove(num)
#for num in nums:
#	if num in one_solution:
#		one_solution.remove(num)	 
#print(one_solution)
#print(len(one_solution))
