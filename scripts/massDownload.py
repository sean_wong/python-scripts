#downloads files that are not in an archive text file

import wget
import re
import requests
import bs4

url = "https://www.flickr.com"
pattern = re.compile(r'') #enter link here
pattern2 = re.compile(r'https://farm[0-9].staticflickr.com/[0-9]*/[0-9]{11}[_0-9a-z]*k.jpg')

with open('../Downloads/html.txt', 'r') as file:
    html = file.read()

links = pattern.findall(html)
links = set(links)
links = list(links)
links.sort()

contents = []

with open("../Pictures/", 'r') as file: #create and open a text file that serves as an archive of downloaded images
    archiveCheck = file.read().split()

archive = open("../Pictures/", "a+")
for i in range(0,len(links)):
    link = url + links[i] + "sizes/k/"
    content = requests.get(link)
    soup = bs4.BeautifulSoup(content.text, 'html.parser')
    stuff = soup.find_all('div', {'id':'allsizes-photo'})[0].img['src']
    if stuff not in archiveCheck:
        contents.append(stuff)
        archive.write(stuff + "\n")

archive.close()

for i in range(0, len(contents)):
        filename = wget.download(contents[i], "../Pictures/") #download photos to pictures folder
