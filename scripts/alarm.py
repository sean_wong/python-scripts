#Sean Wong
#7/12/17

import time
import os

def set_alarm():
    print("It is currently {}".format(time.strftime("%H:%M:%S")))
    while(True):
        try:
            alarm = input("What time would you like to set the alarm? (1:23 AM = 0123): ")
            if int(alarm[0:2]) > 23 | int(alarm[2:4]) > 59: #impossible times
                raise IndexError
            elif int(alarm[0:2]) < 1:
                raise IndexError
            elif len(str(alarm)) < 4: #response is in invalid format
                raise IndexError
            return(alarm)
        except (ValueError, IndexError):
            print("Invalid response, please try again.")

def play_alarm():
    os.system("mpv" + " ~/Downloads/alarm.mp3") #can change alarm sound here
    
def wait(alarm):
    print("Press Control-C to exit the program at any time.")
    while(alarm != time.strftime("%H%M")):
            os.system('clear')
            print(time.strftime("%H:%M:%S"))
            time.sleep(1)

if __name__ == '__main__':
    alarm = set_alarm()
    wait(alarm)
    play_alarm()
