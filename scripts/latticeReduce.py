import math

def length(b):
    return(math.sqrt(((b[0])**2)+((b[1])**2)))

def latticeReduce(b1,b2):
    while length(b1) > length(b2):
        print(b1,b2)
        store = b1
        b1 = b2
        b2 = store
        u = round((b1[0]*b2[0] + b1[1]*b2[1])/((b1[0]**2)+(b1[1]**2)))
        print(u)
        if u == 0:
            return(b1,b2)
        else:
            b2[0] = b2[0]-(u*b1[0])
            b2[1] = b2[1]-(u*b1[1])

print(latticeReduce([90,123],[56,76])) 
