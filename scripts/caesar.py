#prints all possibilities of a caesar cipher

import string

alphabet = string.ascii_uppercase

def cipher(plaintext, shift):
	ciphertext = []
	for letter in plaintext.upper():
		for i in range(0,len(alphabet)):
			if letter == alphabet[i]:
				ciphertext.append(i)
	for i in range(0,len(ciphertext)):
		ciphertext[i] = ciphertext[i] + shift
		if ciphertext[i] > 25:
			ciphertext[i] = ciphertext[i] - 26
	for i in range(0,len(ciphertext)):
		ciphertext[i] = alphabet[ciphertext[i]]
	return ciphertext

#print("Ceasar shift" + "    " + "Encrypted text")
for i in range(0,26):
	print(cipher("COMEHEREATONCE", i))
