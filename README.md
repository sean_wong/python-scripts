# Python Scripts

Somewhat useful python scripts I have written.

1. [alarm clock](scripts/alarm.py): plays a specified sound at a user defined time
2. [tomato](scripts/tomato.py): pomodori technique program; that allows for custom settings
3. [name game](scripts/nameGame.py): creates a verse based on the rules set by the song "Name Game" by Shirley Ellis
4. [caesar cipher](scripts/caesar.py): prints all possibilities of a caesar cipher
5. [lattice reduction](scripts/latticeReduce.py): implement lattice reduction algorithm
6. [pythagorean triple generator](scripts/pythagoreanTripleGeneratorV2.py): generates a pythagorean triplet (3, 4, 5)
7. [arranged probability](scripts/arrangedProbability.py): finds the number of blue discs for which there is 50% chance of taking two blue discs where the number of discs > 1000000000000
8. [special palindromes](scripts/specialPalindromes.py): generate palindromes that are the sum of consecutive squares
9. [mass downloader](scripts/massDownload.py): downloads files and keeps track of whether they were downloaded in an archive file
10. [reddit stream aggregator](scripts/redditStreams.py): collaborative effort in creating a program to aggregate sports streams on reddit
11. [twitch viewer](scripts/twitch.py): displays top streams and games, allowing user to select and view a particular stream
12. [location acquirer](scripts/getLocation.py): prints details about current location